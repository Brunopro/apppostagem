export interface IUser {
    email: string;
    password: string;
    photo?: string;
    name: string;
}
