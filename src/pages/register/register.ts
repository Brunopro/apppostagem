import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';


import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';

import { IUser } from '../../interfaces/IUser';

import { AuthProvider } from '../../providers/auth/auth';
import { AngularFireAuth } from 'angularfire2/auth';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  registerForm: FormGroup;

  user = {} as IUser;

  photoPerfil: string = '../../assets/imgs/photo_perfil.png';


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider,
    private alertCtrl: AlertController,
    public fire: AngularFireAuth,
    public formBuilder: FormBuilder,
  ){
    this.registerForm = this.formBuilder.group({
      name: [null, [Validators.required, Validators.minLength(3)]],
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required, Validators.minLength(6)]]
    })
  }

 loginPage(){
   this.navCtrl.pop()
 }

 register(){
    let user = {} as IUser;

    user.email = this.registerForm.value.email
    user.password = this.registerForm.value.password
    user.name = this.registerForm.value.name
    user.photo = this.photoPerfil;

    this.auth.register(user).then((res) => {
        this.navCtrl.setRoot(HomePage)
    }).catch(
      (erro : any) => {
      if(erro.code == 'auth/weak-password'){
        this.alert('Erro', 'A sua senha deve ter no minimo 6 caracteres.')
      } else if(erro.code == 'auth/email-already-in-use'){
        this.alert('Erro', 'O e-mail digitado já esta em uso.')
      }else if (erro.code == 'auth/invalid-email'){
        this.alert('Erro', 'O e-mail digitado não é valido.')
      }else if(erro.code == 'auth/operation-not-allowed'){
        this.alert('Erro', 'Não esta habilitado criar usúarios.')
      }else{
        this.alert(erro.code, erro.messsage)
      }});
 }



 alert(title, subTitle) {
  let alert = this.alertCtrl.create({
    title: title,
    subTitle: subTitle,
    buttons: ['OK']
  });
  alert.present();
}

}
