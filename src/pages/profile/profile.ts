import { Component } from '@angular/core';
import { IonicPage, NavController} from 'ionic-angular';


import { AngularFireAuth } from 'angularfire2/auth';

import { LoginPage } from '../login/login';
import { IUser } from '../../interfaces/IUser';
import { PreferencesProvider } from '../../providers/preferences/preferences';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  user = {} as IUser;

  constructor(public navCtrl: NavController,
              public fire: AngularFireAuth,
              public preference: PreferencesProvider) {
                preference.get().then((user) => {
                  console.log('home.ts user preference', user)
                  this.user = user;
                })
              }
}
