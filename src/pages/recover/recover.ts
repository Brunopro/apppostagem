import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';


import { IUser } from '../../interfaces/IUser';
import { AuthProvider } from '../../providers/auth/auth';


@IonicPage()
@Component({
  selector: 'page-recover',
  templateUrl: 'recover.html',
})
export class RecoverPage {

  user = {} as IUser;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public auth: AuthProvider,
              public toastCtrl: ToastController,
              public alertCtrl: AlertController) {
  }

  recoverPass(user){
    let toast = this.toastCtrl.create({duration: 3000, position: 'bottom'});

    if( user.email == null ){
      this.alert('Erro', 'Prencha o campo email')
    } else{
    this.auth.recoverPassword(user.email).then(() => {
      toast.setMessage('Soicitação foi enviada para seu email.')
      toast.present();
      this.navCtrl.pop();
    }).catch((erro) => {
      if(erro.code == 'auth/invalid-email'){
        this.alert('Erro', 'O endereço de email é inválido')
      } else {
        this.alert(erro.code, erro.messsage);
      }
    })
  }
  }

  alert(title, subTitle){
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subTitle,
      buttons: ['OK']
    })

    alert.present();
  }

}
