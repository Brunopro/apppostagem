import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';


import { AngularFireAuth } from 'angularfire2/auth';

import { LoginPage } from '../login/login';
import { PreferencesProvider } from '../../providers/preferences/preferences';
import { WordpressService } from '../../providers/service/wordpress.service';

import { PostPage } from '../post/post'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  posts: Array<any> = new Array<any>();

  morePageAvailable: boolean = true;

  constructor(public navCtrl: NavController,
    public fire: AngularFireAuth,
    public preference: PreferencesProvider,
    public loadingCtrl: LoadingController,
    public wordpressService: WordpressService) {
  }
  ionViewWillEnter() {
    console.log('post length', this.posts.length)
    console.log('post', this.posts)
    console.log()
   
    this.morePageAvailable = true
    if (this.posts.length == 0) {
      console.log('ionViewWillEnter, primeiro if')
      let loading = this.loadingCtrl.create({
        content: 'Carregando'
      });
      loading.present();

      this.wordpressService.getRecentPosts()
        .subscribe(data => {
          for (let post of data) {
            post.excerpt.rendered = post.excerpt.rendered.split('<a')[0] + "<p>";

            this.posts.push(post);
          }
          loading.dismiss();
          console.log('array posts', this.posts[1])
        });
    }
  }

  postTapped(event, post){
    this.navCtrl.push(PostPage, {
      item: post
    })

    console.log('item que está sendo enviado pra post', post)
  }


  loading() {
    let loading = this.loadingCtrl.create({
      content: 'Carregando'
    });
    loading.present();
    return loading;
  }



  logout() {
    this.preference.remove();
    this.fire.auth.signOut();
    this.navCtrl.setRoot(LoginPage)
  }

  doInfinite(infiniteScroll){
    let page = (Math.ceil(this.posts.length/10)) + 1;
    let loading = true;

    this.wordpressService.getRecentPosts(page).subscribe(data => {
      for(let post of data){
        if(!loading){
          infiniteScroll.complete();
        }
        this.posts.push(post);
        loading = false;
      }
    }, err => 
        this.morePageAvailable = false
    )
  }

  doRefresh(refresher){
    //atualiza pagina sem precisa de observable
    this.navCtrl.setRoot(this.navCtrl.getActive().component);

    setTimeout(() => {
      refresher.complete();
    }, 2000)
  }

  

}
