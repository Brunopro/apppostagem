import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';

import { AngularFireAuth } from 'angularfire2/auth'
import { PreferencesProvider } from '../providers/preferences/preferences';
import { AboutPage } from '../pages/about/about';
import { ProfilePage } from '../pages/profile/profile';

import { Keyboard } from '@ionic-native/keyboard';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  pages: Array<{title: string, component: any}>;

  constructor( public platform: Platform,                 
               public statusBar: StatusBar,
               public splashScreen: SplashScreen,
              fire: AngularFireAuth,
              public preferences: PreferencesProvider,
              public keyboard: Keyboard) {

    
    //verifica se tem usuario logado se tiver manda pra HomePage
    const authObserver = fire.authState.subscribe(user => {
      if(user){
        this.rootPage = HomePage;
        authObserver.unsubscribe();
      } else {
        this.rootPage = LoginPage;
        authObserver.unsubscribe();
      }
    })

    this.pages = [
      { title: 'Dicas', component: HomePage },
      { title: 'Minha Conta', component: ProfilePage },
      { title: 'Sobre', component: AboutPage },
    ];
  }
    initializeApp() {
    this.platform.ready().then(() => {
      this.preferences.get().then(user => user ? this.rootPage = HomePage : this.rootPage = LoginPage)
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logout(){
    console.log('clicado')
    this.rootPage = LoginPage;
    this.preferences.remove();
  }
}

//https://www.boatos.org/wp-content/uploads/2017/12/Primeira-foto-que-aparece-do-perfil-do-WhatsApp.jpg
