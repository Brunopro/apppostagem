import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, NavController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login'
import { RegisterPage } from '../pages/register/register';
import { AuthProvider } from '../providers/auth/auth';

import { HttpClientModule } from '@angular/common/http';

import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireModule } from '@angular/fire';
import { config } from '../config';
import { RecoverPage } from '../pages/recover/recover';
import { PreferencesProvider } from '../providers/preferences/preferences';
import { IonicStorageModule } from '@ionic/storage';
import { ProfilePage } from '../pages/profile/profile';
import { WordpressService } from '../providers/service/wordpress.service';
import { HttpModule } from '@angular/http';
import { PostPage } from '../pages/post/post';
import { AboutPage } from '../pages/about/about';
import { Keyboard } from '@ionic-native/keyboard';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    RecoverPage,
    ProfilePage,
    PostPage,
    AboutPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(config),
    AngularFireAuthModule,
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    RecoverPage,
    ProfilePage,
    PostPage,
    AboutPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AuthProvider,
    PreferencesProvider,
    WordpressService,
    Keyboard,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
   
  ]
})
export class AppModule {}
