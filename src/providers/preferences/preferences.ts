import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

//Necessario importar para implementar o storage
import { Storage } from "@ionic/storage";

@Injectable()
export class PreferencesProvider {

  constructor(public http: HttpClient,
              private storage: Storage) {

  }

  create(user): Promise<any>  {
    return this.storage.set('user', user);
   }

   //Vai guardar o user mas antes vai ver se no appmodule ta pronto
   get(): Promise<any> {
     return this.storage.ready()
       .then(() => {

         return this.storage.get('user')
       })


   }

   // Quando deslogar deve remover do storage
   //usar o preference para deslogar
   remove(): Promise<boolean> {
     return this.storage.remove('user')
     .then(() => {
       return true
     })
   }





}
