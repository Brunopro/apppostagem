import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

//Para fazer login e registrar
import { AngularFireAuth } from 'angularfire2/auth';

//para logar com o facebook importar firebase
import firebase from 'firebase';
import { PreferencesProvider } from '../preferences/preferences';
import { EmailValidator } from '@angular/forms';


@Injectable()
export class AuthProvider {

  constructor(public http: HttpClient,
              public fire: AngularFireAuth,
              public preference: PreferencesProvider) {
  }

  register(user){
    return this.fire.auth.createUserWithEmailAndPassword(user.email, user.password).then(() => {
      this.preference.create(user);
    });
  }

  login(user){
    return this.fire.auth.signInWithEmailAndPassword(user.email, user.password);
  }

  loginWithFacebook(){
    return this.fire.auth.signInWithPopup( 
      new firebase.auth.FacebookAuthProvider()).then(() => {
        let user = {
          name: this.fire.auth.currentUser.displayName,
          email: this.fire.auth.currentUser.email,
          photo: this.fire.auth.currentUser.photoURL
        };

        this.preference.create(user);
    })
  }

  loginAnonimo(){
    return this.fire.auth.signInAnonymously().then((data) => {
      console.log('loginAnonimo do auth, data:', data);
      let user = {
        name:'Visitante',
        email: 'visitante@email.com',
        photo: '../assets/imgs/photo_perfil.png'
      };
      this.preference.create(user);
    })
  }

  recoverPassword(email){
    return this.fire.auth.sendPasswordResetEmail(email)
  }




}
